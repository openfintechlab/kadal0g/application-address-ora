
import OracleDBInteractionUtil          from "..//utils/OracleDBInteractionUtil";
import util                             from "util";
import { v4 as uuidv4 }                 from "uuid";
import  logger                          from "../utils/Logger";
import {PostRespBusinessObjects}        from "./bussObj/Response";

export default class ApplicationAddController{
    SQLStatements = {
        "INSERT_SQL01_INTO_APPCTRL": "INSERT INTO application_address (address_id,application_id,address_type,address,add_prop_uri,created_on,updated_on,created_by, updated_by) VALUES (:v0,:v1,:v2,:v3,:v4,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'SYSTEM','SYSTEM')",
        "SELECT_SQL02_FROM_APPCTRL": "select application_id,address_id from application_address where application_id = :v1 AND address_id=:v2",
        "DELETE_SQL03_FROM_APPCTRL": "DELETE from application_address where address_id=:v2",
        "SELECT_SQL04_ON_ADDID": "SELECT address_id,application_id,address_type,address,add_prop_uri,created_on,updated_on,created_by, updated_by from application_address where address_id=:v1",
        "SELECT_SQL05_ON_APPID_ADDTYPE": "SELECT address_id,application_id,address_type,address,add_prop_uri,created_on,updated_on,created_by, updated_by from application_address where application_id=COALESCE(:v1,application_id) OR address_type=COALESCE(:v2,address_type)",
        "UPDATE_SQL06_ON_APPID_ADDID" : "UPDATE application_address SET    address_type = :v1,    address = :v2,    add_prop_uri = :v3,        updated_on = CURRENT_TIMESTAMP,        updated_by = 'SYSTEM' WHERE  address_id = :v4 	AND application_id = :v5"
    };

    constructor(private referenceID: string){}

    /**
     * Creates Application-Address 
     * @param {any} request JSON request object
     */
    public async create(request:any){
        logger.debug(`[${this.referenceID}]: Request Received for creating application: ${util.inspect(request,{compact:true,colors:true, depth: null})}`);        
        try{
            let result = await OracleDBInteractionUtil.executeRead(this.SQLStatements.SELECT_SQL02_FROM_APPCTRL,[request["application-address"].application_id,request["application-address"].address_id]);
            if(result.rows.length > 0){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8152","Application already exist");
            }
            let binds = [
                request["application-address"].address_id,
                request["application-address"].application_id,
                request["application-address"].address_type,
                request["application-address"].address,
                request["application-address"].add_prop_uri,
            ];
            let dbResult = await OracleDBInteractionUtil.executeUpdate(this.SQLStatements.INSERT_SQL01_INTO_APPCTRL, binds);
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");
        }catch(error){
            logger.error(`${this.referenceID} Error adding application in the db: ${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            if(error.metadata){
                throw error;
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8503","Error While performing the Operation");
            }            
        }
    }

    /**
     * Updates Application-Address 
     * @param {any} request JSON request object
     */
    public async update(request:any){
        logger.debug(`[${this.referenceID}]: Request Received for creating application: ${util.inspect(request,{compact:true,colors:true, depth: null})}`);        
        try{
            let result = await OracleDBInteractionUtil.executeRead(this.SQLStatements.SELECT_SQL02_FROM_APPCTRL,[request["application-address"].application_id,request["application-address"].address_id]);
            if(result.rows.length <= 0){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8153","Required key does not exist in the db");   
            }
            let binds = [                
                request["application-address"].address_type,
                request["application-address"].address,
                request["application-address"].add_prop_uri,
                request["application-address"].address_id,
                request["application-address"].application_id
            ];
            let dbResult = await OracleDBInteractionUtil.executeUpdate(this.SQLStatements.UPDATE_SQL06_ON_APPID_ADDID, binds);
            
            return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");
        }catch(error){
            logger.error(`${this.referenceID} Error adding application in the db: ${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            if(error.metadata){
                throw error;
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8503","Error While performing the Operation");
            }            
        }
    }


    /**
     * Delete specific instance of the application address
     * @param application_id Application ID
     * @param address_id Address ID
     */
    public async delete(address_id: string){
        try{
            let dbResult = await OracleDBInteractionUtil.executeUpdate(this.SQLStatements.DELETE_SQL03_FROM_APPCTRL,[address_id]);
            if(dbResult.rowsAffected > 0){
                return new PostRespBusinessObjects.PostingResponse().generateBasicResponse("0000","Success!");   
            }                
            else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8153","Required key does not exist in the db");   
            }
        }catch(error){
            logger.error(`${this.referenceID} Error adding applicattion in the db: ${util.inspect(error,{compact:true,colors:true, depth: null})}`);
            if(error.metadata){
                throw error;
            }else{
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8503","Error While performing the Operation");
            }   
        }
    }

    public async getOnAdd(add_id:string){
        try{
            let dbresult = await OracleDBInteractionUtil.executeRead(this.SQLStatements.SELECT_SQL04_ON_ADDID,[add_id]);
            return await this.createBusinessObject(dbresult);
        }catch(error){
            logger.error(`Error occured while converting data to business object ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
            if(error.metadata === undefined){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8151","Error While performing the GET Operation");   
            }else{
                throw error;
            }
        }
    }

    /**
     * Get all Address type based on the Application and address type(optional)
     * @param {string} app_id Application ID
     * @param {string} address_type (optional) Address type
     */
    public async get(app_id:string,address_type?:string){
        try{
            let dbresult = await OracleDBInteractionUtil.executeRead(this.SQLStatements.SELECT_SQL05_ON_APPID_ADDTYPE,[app_id,address_type]);
            return await this.createBusinessObject(dbresult);
        }catch(error){
            logger.error(`Error occured while converting data to business object ${util.inspect(error,{compact:true,colors:true, depth: null})}`)
            if(error.metadata === undefined){
                throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8151","Error While performing the GET Operation");   
            }else{
                throw error;
            }
        }
    }

    private async createBusinessObject(dbResult:any){
        return new Promise<any>((resolve:any, reject:any) => {
            if(dbResult.rows.length <= 0){
                reject(new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8153","Required key does not exist in the db"));
            }
            var bussObj:any = {
                metadata: {
                    "status": '0000',
                    "description": 'Success!',
                    "responseTime": new Date(),                    
                },
                "application-address": []
            }
            dbResult.rows.forEach((row:any) => {
                bussObj['application-address'].push({
                    "address_id": row.ADDRESS_ID,
                    "address_type":row.ADDRESS_TYPE,
                    "application_id": row.APPLICATION_ID,
                    "address": row.ADDRESS,
                    "add_prop_uri": row.ADD_PROP_URI
                });
            });
            resolve(bussObj);
        } ); 
    }
}