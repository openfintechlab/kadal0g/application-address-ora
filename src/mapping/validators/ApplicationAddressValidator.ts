import Joi                              from "joi";
import logger                           from "../../utils/Logger"
import {PostRespBusinessObjects}        from "../bussObj/Response";
import AppConfig                        from "../../config/AppConfig";

export default class ApplictionAddressValidator{
    ApplicationAddressSchemaModel = Joi.object({
        "application-address": {
            "address_id": Joi.string().min(8).max(56).required(),
            "application_id": Joi.string().min(8).max(56).required(),
            "address_type":Joi.string().min(2).max(128).required(),
            "address": Joi.string().min(8).max(256).required(),
            "add_prop_uri": Joi.string().min(8).max(256)
        }
    });

    ApplicationAddressParamsSchema = Joi.object({
        "app_id": Joi.string().min(8).max(56).optional(),
        "add_id": Joi.string().min(8).max(56).required()
    });

    public validate(request:any){
        let {error, value} = this.ApplicationAddressSchemaModel.validate(request, { presence: "required" } );
        if(error){
            logger.error(`Error while validating the business object: ${error.message}`);
            this.getValidationError(error);
        }else{
            return value;
        }
    }

    public validateParams(params:any){
        let {error, value} = this.ApplicationAddressParamsSchema.validate(params,{ presence: "required" } );
        if(error){
            logger.error(`Error while validating the business object: ${error.message}`);
            this.getValidationError(error);
        }else{
            return value;
        }
    }

    private getValidationError(error:any){
        if(AppConfig.config.OFL_MED_NODE_ENV === 'debug'){
            let trace:PostRespBusinessObjects.Trace = new PostRespBusinessObjects.Trace("schema-validation",error.message);
            throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8502","Schema validation error", trace);   
        }else{
            throw new PostRespBusinessObjects.PostingResponse().generateBasicResponse("8502","Schema validation error");   
        }           
    }
}

/**
 *  address_id                    VARCHAR2(56 CHAR) NOT NULL,
    application_id                VARCHAR2(56 CHAR) NOT NULL,
    address_type                  VARCHAR2(64 CHAR) NOT NULL,
    address                       VARCHAR2(256 CHAR) NOT NULL,
    add_prop_uri                  VARCHAR2(512 CHAR),
    created_on                    TIMESTAMP(9) WITH TIME ZONE DEFAULT current_timestamp NOT NULL,
    updated_on                    TIMESTAMP(9) WITH TIME ZONE DEFAULT current_timestamp,
    created_by                    VARCHAR2(64 CHAR) NOT NULL,
    med_service_application_name  VARCHAR2(128 CHAR) NOT NULL
 */